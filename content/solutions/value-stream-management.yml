---
  title: Value Stream Management
  description: GitLab's Value Stream Management provides a systematic method to reduce time to value, optimize for business outcomes, and improve software quality.
  components:
    - name: 'solutions-hero'
      data:
        title: Value Stream Management
        subtitle: Measure and manage the business value of your DevSecOps lifecycle.
        aos_animation: fade-down
        aos_duration: 500
        img_animation: zoom-out-left
        img_animation_duration: 1600
        primary_btn:
          text: Start your free trial
          url: /free-trial/
        # TODO: Change for actual image
        image:
          image_url: /nuxt-images/solutions/infinity-icon-cropped.svg
          image_url_mobile: /nuxt-images/solutions/no-image-mobile.svg
          alt: "Image: gitlab + value stream management"
    - name: copy-media
      data:
        block:
          - header: Benefits of Value Stream Management
            miscellaneous: |
              Software development should always aim to maximize customer or business value delivery—but how do you identify inefficiencies in that delivery, and how can you course-correct value when you do? GitLab’s Value Stream Management helps businesses visualize their end-to-end DevOps workstream, identify and target waste and inefficiencies, and take action to optimize those workstreams to deliver the highest possible velocity of value.
          - header: A Value Stream Delivery Platform
            miscellaneous: |
              In [The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams](https://www.gartner.com/en/documents/3979558/the-future-of-devops-toolchains-will-involve-maximizing){data-ga-name="The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams" data-ga-location="body"},” Gartner recommended that "infrastructure and operations leaders responsible for selecting and deploying DevOps toolchains should: drive business ability by using DevOps value stream delivery platforms that reduce the overhead of managing complex toolchains."1 By providing an [entire DevOps platform](/solutions/devops-platform/){data-ga-name="GitLab - The DevOps Platform" data-ga-location="body"} The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams as a single application, GitLab is uniquely suited to provide end-to-end visibility throughout the entire lifecycle without the “toolchain tax.” As the place where work happens, GitLab can also unite visualization with action, allowing users to jump from learning to doing at any time, without losing context.

              *1 Gartner "The Future of DevOps Toolchains Will Involve Maximizing Flow in IT Value Streams," Manjunath Bhat, et al, 14 January 2020 (Gartner subscription required)*
            media_link_href: https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html
            media_link_text: Value Stream Analytics
            media_link_data_ga_name: Value Stream Analytics
            media_link_data_ga_location: body
          - header: View and manage end-to-end processes
            text: |
              Value Streams Analytics helps you visualize and manage the DevOps flow from ideation to customer delivery. Out of the box, GitLab offers actionable reporting on common workflows and metrics, with nothing to install or configure. If you want to dive deeper or model custom workflows, GitLab’s unified, comprehensive data store makes it easy to track whatever events matter.

              *   **One tool, no chain:** GitLab unifies the entire DevOps toolchain into a single application. There are no integrations to manage, no API chokepoints to limit visibility, and a shared experience for everyone in the company, regardless of role.

              *   **Shared, actionable data:** A single source of insight built on a single system of work means you can spend more time developing value and less time finding where it’s stalled.

              *   **Focus on value - not ceremony:** Track and manage the actual flow of work and eliminate unnecessary ceremony. A single DevOps application means single-click drill down into actual work items, so you can remove blockages as soon as you find them.

            image:
              image_url: /nuxt-images/solutions/VSA-DORA-15.0.png
              alt: ""
              caption: |
                [Model your process](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"} with [customizable Value Streams](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#customizable-value-stream-analytics){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"} (since 12.9)
            inverted: true
          - header: Measure
            text: |
              Drive continuous improvement based on the data from your value stream.

              * **Track flow and accelerate:** Focusing on the flow of value to customers is key to streamlining the delivery process.
              * **Cycle time:** Track the actual work and effort, the real cycle time to deliver value.
              * **Business value:** Link actual value back to the change.
              * **DORA4 metrics:** DORA4 metrics help you benchmark your DevOps maturity and are a useful indicator for comparison - either within the team or comparable companies / industries. Monitor the [Lead time for change](https://docs.gitlab.com/ee/api/dora4_project_analytics.html#list-project-merge-request-lead-times){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"} and [Deployment Frequency](https://docs.gitlab.com/ee/api/dora4_project_analytics.html){data-ga-name="DORA4 Analytics Project API | GitLab" data-ga-location="body"} to measure your DevOps process efficency.
            image:
              image_url: /nuxt-images/solutions/tasks-by-type.png
              alt: ""
              caption: |
                [See distribution of business value delivered across types of work to better align execution with strategy.](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#type-of-work---tasks-by-type-chart){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"}
          - header: Manage
            text: |
              The value streams help you visualize and manage the flow of new innovation from idea to customers.

              * **Identify constraints**: Find the sources of friction in your value stream and optimise lead time.
              * **Remove bottlenecks:** Take action to address constraints.
              * **Optimize flow:** Accelerate DevOps best practices across your organization.
              * **Manage Efficiency:** [Detailed insights](https://docs.gitlab.com/ee/user/project/insights/){data-ga-name="Value Stream Analytics | GitLab" data-ga-location="body"} showcasing your triage hygiene, issues / bugs created, time to merge and many more enable you to identify and fix inefficiencies in your development process with ease.
            image:
              image_url: /nuxt-images/solutions/days-to-complete.png
              alt: ""
              caption: |
                [Inspect and improve processes, spot trends, and perform root cause on outliers.](https://docs.gitlab.com/ee/user/analytics/value_stream_analytics.html#days-to-completion-chart){data-ga-name="Insights | GitLab" data-ga-location="body"}
              vertical_centered: true
            inverted: true
    - name: 'solutions-carousel-videos'
      data:
        title: Value Stream Management
        videos:
          - title: "VSM Executive Insights"
            photourl: /nuxt-images/video-carousels/vsm_1/1.jpg
            video_link: https://www.youtube-nocookie.com/embed/0MR7Q8Wiq6g
            carousel_identifier:
              - ''

          - title: "VSM Developer Insights"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/JPYWrRByAYk
            carousel_identifier:
              - ''

          - title: "VSM Security Insights"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/KSJ9VhKQAS0
            carousel_identifier:
              - ''

          - title: "VSM Operations Insights"
            photourl: /nuxt-images/video-carousels/vsm_1/4.jpg
            video_link: https://www.youtube-nocookie.com/embed/lDWxH2YO3Yk
            carousel_identifier:
              - ''

          - title: "VSM Productivity Insights"
            photourl: /nuxt-images/video-carousels/vsm_1/5.jpg
            video_link: https://www.youtube-nocookie.com/embed/vYKC7r8ztVA
            carousel_identifier:
              - ''

          - title: "VSM Issue Boards for Mapping"
            photourl: /nuxt-images/video-carousels/vsm_1/6.jpg
            video_link: https://www.youtube-nocookie.com/embed/9ASHiQ2juYY
            carousel_identifier:
              - ''

          - title: "VSM Business Value Monitoring"
            photourl: /nuxt-images/video-carousels/vsm_1/7.jpg
            video_link: https://www.youtube-nocookie.com/embed/oG0VESUOFAI
            carousel_identifier:
              - ''


