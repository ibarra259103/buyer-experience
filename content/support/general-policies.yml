---
title: Support General Policies
description: Support General Policies
support-hero:
  data:  
    title: Support General Policies
side_menu:
  anchors:
    text: "ON THIS PAGE"
    data:
      - text: "Differences Between Support Tickets and GitLab Issues"
        href: "#differences-between-support-tickets-and-gitlab-issues"                    
      - text: "We don't keep tickets open (even if the underlying issue isn't resolved)"
        href: "#we-dont-keep-tickets-open-even-if-the-underlying-issue-isnt-resolved"
      - text: "Issue Creation"
        href: "#issue-creation"
      - text: "Working Effectively in Support Tickets"
        href: "#working-effectively-in-support-tickets"
      - text: "Automated Follow-up for Pending Cases"
        href: "#automated-follow-up-for-pending-cases"
      - text: "Please don't use language intended to threaten or harass"
        href: "#please-dont-use-language-intended-to-threaten-or-harass"
      - text: "Please don't send encrypted messages"
        href: "#please-dont-send-encrypted-messages"
      - text: "Please don't send files in formats that can contain executable code"
        href: "#please-dont-send-files-in-formats-that-can-contain-executable-code"
      - text: "Please don't share login credentials"
        href: "#please-dont-share-login-credentials"
      - text: "Do not contact Support Engineers directly outside of issues or support cases"
        href: "#do-not-contact-support-engineers-directly-outside-of-issues-or-support-cases"
      - text: "Sanitizing data attached to Support Tickets"
        href: "#sanitizing-data-attached-to-support-tickets"
      - text: "Support for GitLab on restricted or offline networks"
        href: "#support-for-gitlab-on-restricted-or-offline-networks"
      - text: "Including colleagues in a support ticket"
        href: "#including-colleagues-in-a-support-ticket"
      - text: "Solving Tickets"
        href: "#solving-tickets"
      - text: "Can Users Solve Tickets Themselves?"
        href: "#can-users-solve-tickets-themselves"
      - text: "GitLab Instance Migration"
        href: "#gitlab-instance-migration" 
      - text: "Handling Unresponsive Tickets"
        href: "#handling-unresponsive-tickets"                                                                                                                                                       
  hyperlinks:
    text: ''
    data: []
components:
  - name: support-copy
    data:
      block:
      - subtitle: 
          text: Differences Between Support Tickets and GitLab Issues
          id: differences-between-support-tickets-and-gitlab-issues
        text: |
          <p>
            It's useful to know the difference between a support ticket opened through our 
            <a href="https://support.gitlab.com/">Support Portal</a> vs. <a href="https://gitlab.com/gitlab-org/gitlab/-/issues">an issue on GitLab.com</a>.
          </p>
          <br />
          <p>
            For customers with a license or subscription that includes support, always feel free to contact support with your issue in the first instance via 
            <a href="https://support.gitlab.com/">the Support Portal</a>. This is the primary channel the support team uses to interact with customers and it 
            is the only channel that is based on an SLA. Here, the GitLab support team will gather the necessary information and help debug the issue. 
            In many cases, we can find a resolution without requiring input from the development team. However, sometimes debugging will uncover a bug in 
            GitLab itself or that some new feature or enhancement is necessary for your use-case.
          </p>
          <br />
          <p>
            This is when we create an <a href="https://gitlab.com/gitlab-org/gitlab/-/issues">issue on GitLab.com</a> - whenever input is needed from developers to investigate an issue further, to fix a bug, or to consider 
            a feature request. In most cases, the support team can create these issues on your behalf and assign the correct labels. Sometimes we ask the customer 
            to create these issues when they might have more knowledge of the issue or the ability to convey the requirements more clearly. 
            Once the issue is created on GitLab.com it is up to the product managers and engineering managers to prioritize and drive the fix or feature to completion.
             The support team can only advocate on behalf of customers to reconsider a priority. Our involvement from this point on is more minimal.
          </p>
      - subtitle:
          text: We don't keep tickets open (even if the underlying issue isn't resolved)
          id: we-dont-keep-tickets-open-even-if-the-underlying-issue-isnt-resolved
        text: |
          <p>
            Once an issue is handed off to the development team through an issue in a GitLab tracker, the support team will close out the support ticket as 
            Solved even if the underlying issue is not resolved. This ensures that issues remain the single channel for communication: customers, developers 
            and support staff can communicate through only one medium.
          </p>
      - subtitle:
          text: Issue Creation
          id: issue-creation
        text: |
          <p>
            Building on the above section, when bugs, regressions, or any application behaviors/actions <strong>not working as 
            intended</strong> are reported or discovered during support interactions, 
            the GitLab Support Team will create issues in GitLab project repositories on behalf of our customers.
          </p>
          <br />
          <p>
            For feature requests, both involving the addition of new features as well as the change of features currently <strong>working as intended</strong>, 
            support will request that the customer create the issue on their own in the appropriate project repos.
          </p>
      - subtitle:
          text: Working Effectively in Support Tickets
          id: working-effectively-in-support-tickets
        text: |
          <p>As best you can, please help the support team by communicating the issue you're facing, or question you're asking, with as much detail as available to you. Whenever possible, include:</p>
          <ul>
            <li><a href="https://docs.gitlab.com/ee/administration/logs.html#gathering-logs">log files relevant</a> to the situation.</li>
            <li>steps that have already been taken towards resolution.</li>
            <li>relevant environmental details, such as the architecture.</li>
          </ul>
          <p>
            We expect for non-emergency tickets that GitLab administrators will take 20-30 minutes to formulate the support ticket with relevant information. 
            A ticket without the above information will reduce the efficacy of support.
          </p>
          <br />
          <p>
            In subsequent replies, the support team may ask you follow-up questions. Please do your best to read through the entirety of the reply and answer any such questions. 
            If there are any additional troubleshooting steps, or requests for additional information please do your best to provide it.
          </p>
          <br />
          <p>
            The more information the team is equipped with in each reply will result in faster resolution. For example, if a support engineer has to ask for logs, it will result in more cycles. 
            If a ticket comes in with everything required, multiple engineers will be able to analyze the problem and will have what is necessary to further escalate to developers if so required.
          </p>
      - subtitle:
          text: Automated Follow-up for Pending Cases
          id: automated-follow-up-for-pending-cases
        text: |
          <p>
            The support teams strive to provide quick and efficient responses to technical challenges. In order to help gather the necessary details and efficiently troubleshoot issues it may 
            be necessary to request some additional information from case submitter. When this is done the ticket will go into a pending state to indicate support is waiting to hear back with the results of
            the troubleshooting or more information. After 7 days in <code>pending</code>, an automated follow-up will occur reminding the case's participants that the case is waiting for a response with additional details. 
            If the issue is resolved you can <a href="/support/general-policies/#can-users-solve-tickets-themselves">solve the case yourself</a>, otherwise the case will solve itself out in another 7 days. If the issue is not resolved by the information the support engineer had provided, 
            you can reply to the case and provide the current status of the troubleshooting, along with any information the engineer may have requested, and the case will continue once more.
          </p>
      - subtitle:
          text: Please don't use language intended to threaten or harass
          id: please-dont-use-language-intended-to-threaten-or-harass
        text: |
          <p>
            If your ticket contains language that goes against the <a href="/community/contribute/code-of-conduct/">GitLab Community Code of Conduct</a>, you'll receive the following response and have your ticket closed:
          </p>
          <br />
          <blockquote>
            <p>Hello,</p>
            <br />
            <p>While we would usually be very happy to help you out with any issue, we cannot assist you on this ticket due to the language used not adhering to the <a href="/community/contribute/code-of-conduct/">GitLab Community Code of Conduct</a>. As noted in our <a href="/support/general-policies/#please-dont-use-language-intended-to-threaten-or-harass">Statement of Support</a>, 
            we're closing this ticket.</p>
            <br />
            <p>Please create a new ticket that complies with our guidelines and one of our support engineers will be happy to assist you.</p>
            <br />
            <p>Thank you,</p>
          </blockquote>
          <br />
          <p>Repeat violations may result in termination of your support contract and/or business relationship with GitLab.</p>
      - subtitle:
          text: Please don't send encrypted messages
          id: please-dont-send-encrypted-messages
        text: |
          <p>
            Some organizations use 3rd party services to encrypt or automatically expire messages. As a matter of security, 
            please do not include any sensitive information in the body or attachments in tickets. All interactions with 
            GitLab Support should be in plain-text.
          </p>
          <br />
          <p>
            GitLab Support Engineers will not click on URLs contained within tickets or interact with these type of 3rd party 
            services to provide support. If you do send such a reply, an engineer will politely ask you to submit your support 
            ticket in plain text, and link you to this section of our Statement of Support.
          </p>
          <br />
          <p>
            If there's a pressing reason for which you need to send along an encrypted file or communication, please discuss it 
            with the Engineer in the ticket.
          </p>
      - subtitle:
          text: Please don't send files in formats that can contain executable code
          id: please-dont-send-files-in-formats-that-can-contain-executable-code
        text: |
          <p>
            Many common file formats, such as Microsoft Office files and PDF, can contain executable code which can be run automatically when 
            the file is opened. As a result, these types of files are often used a computer attack vector.
          </p>
          <br />
          <p>
            GitLab Support Engineers will not download files or interact with these type of files. If you send a file in a format of concern, an engineer 
            will ask you to submit your attachments in an accepted format, and link you to this section of the Support page.
          </p>
          <br />
          <p>
            Examples of acceptable formats include:
          </p>
          <ul>
            <li>Plain Text files (.txt, .rtf, .csv, .json, yaml)</li>
            <li>Images (.jpg, .gif, .png)</li>
          </ul>
          <p>
            Support users can quickly convert PDF files to benign images prior to attaching them to the support case using 
            the <a href="https://linux.die.net/man/1/pdftoppm">pdftoppm</a> utility. Ex: <code>pdftoppm -png path/to/file.pdf output_file_name</code>
          </p>
      - subtitle: 
          text: Please don't share login credentials 
          id: please-dont-share-login-credentials
        text: |
          <p>
            Do not share login credentials for your GitLab instance to the support team. If the team needs more information about the problem, 
            we will offer to schedule a call with you.
          </p>
      - subtitle: 
          text: Do not contact Support Engineers directly outside of issues or support cases
          id: do-not-contact-support-engineers-directly-outside-of-issues-or-support-cases
        text: |
          <p>
            GitLab Support engages with users via the GitLab Support Portals and GitLab.com issues or merge requests only. Using these scoped methods 
            of communication allows for efficiency and collaboration, and ensures both parties' safety while addressing problems that may arise.
            No attempts should be made to receive support via direct individual email, social media, or other communication methods used by GitLab team members. 
            Use of these unofficial methods to obtain support may be considered abuse of the platform. Repeat violations may result in termination of your support contract and/or business relationship with GitLab.
          </p>
          <br />
          <p>
            If you contact a Support Engineer directly, they'll reply with a short explanation and a link to this paragraph.
          </p>
      - subtitle:
          text: Sanitizing data attached to Support Tickets
          id: sanitizing-data-attached-to-support-tickets
        text: |
          <p>
            If relevant to the problem and helpful in troubleshooting, a GitLab Support Engineer will request information regarding configuration files or logs.
          </p>
          <br />
          <p>
            We encourage customers to sanitize all secrets and private information before sharing them in a GitLab Support ticket.
          </p>
          <br />
          <p>Sensitive items that should never be shared include:</p>
          <ul>
            <li>credentials</li>
            <li>passwords</li>
            <li>tokens</li>
            <li>keys</li>
            <li>secrets</li>
          </ul>
          <p>
            There's more specific information on the dedicated <a href="/support/sensitive-information">handling sensitive information with GitLab Support</a> page.
          </p>
      - subtitle:
          text: Support for GitLab on restricted or offline networks
          id: support-for-gitlab-on-restricted-or-offline-networks
        text: |
          <p>
            GitLab Support may request logs in Support tickets or ask you to screenshare in customer calls if it would be the most efficient and effective way to troubleshoot and solve the problem.
          </p>
          <br />
          <p>
            Under certain circumstances, sharing logs or screen sharing may be difficult or impossible due to our customers' internal network security policies.
          </p>
          <br />
          <p>
            GitLab Support will never ask our customers to violate their internal security policies, but Support Engineers do not know the details of our customers' internal network security policies.
          </p>
          <br />
          <p>
            In situations where internal or network security policies would prevent you from sharing logs or screen sharing with GitLab Support, please communicate this as early as possible in the ticket 
            so we can adjust the workflows and methods we use to troubleshoot.
          </p>
          <br />
          <p>
            Customer policies preventing the sharing of log or configuration information with Support may lengthen the time to resolution for tickets.
          </p>
          <br />
          <p>
            Customer policies preventing screen sharing during GitLab Support customer calls may impact a Support Engineer's ability to resolve issues during a scheduled call.
          </p>
      - subtitle:
          text: Including colleagues in a support ticket
          id: including-colleagues-in-a-support-ticket
        text: |
          <p>
            Our Support Portal will automatically drop any CCed email addresses for tickets that come in via email. If you would like to include colleagues in a support interaction, 
            you must be logged into the Support Portal. From there, the option to add your colleagues as CCs will be available.
          </p>
          <br />
          <p>
            To view tickets you have been CCed on, navigate to <strong>Profile Icon > My activities > Requests I'm CC'd on</strong>.
          </p>
      - subtitle:
          text: Solving Tickets
          id: solving-tickets
        text: |
          <p>
            If a customer explains that they are satisfied their concern is being addressed properly in an issue created on their behalf, then the conversation should continue within the issue itself, 
            and GitLab support will close the support ticket. Should a customer wish to reopen a support ticket, they can simply reply to it and it will automatically be reopened.
          </p>
      - subtitle:
          text: Can Users Solve Tickets Themselves? 
          id: can-users-solve-tickets-themselves
        text: |
          <p>
            A user can solve a ticket themselves by logging into the Support Portal and navigating to the <a href="https://support.gitlab.com/hc/en-us/requests">My activities</a> page. From there the user can select one of their tickets that is in an open or 
            pending state, navigate to the bottom of the ticket notes section, check the <code>Please consider this request solved</code> box and press <code>Submit</code>. If the user would like to reopen the case they can simply
            respond with a new comment and the case will reopen or create a follow up case.
          </p>
      - subtitle:
          text: GitLab Instance Migration
          id: gitlab-instance-migration
        text: |
          <p>
            If a customer requests assistance in migrating their existing self-hosted GitLab to a new instance, you can direct them to our <a href="https://docs.gitlab.com/ee/user/project/import/#migrating-between-two-self-hosted-gitlab-instances">Migrating between two self-hosted GitLab Instances</a> documentation. Support will assist with any issues that arise from the 
            GitLab migration. However, the setup and configuration of the new instance, outside of GitLab specific configuration, is considered out of scope and Support will not be able to assist with any resulting issues.
          </p>
      - subtitle:
          text: Handling Unresponsive Tickets
          id: handling-unresponsive-tickets
        text: |
          <p>
            To prevent an accumulation of tickets for which we have not received a response within a specific timescale, we have established the following process. If the ticket owner (our customer) fails to respond within 20 days following our last reply, our ticketing system will mark the ticket as solved. If the ticket owner (our customer) fails to respond within 7 days of 
            the ticket being marked as solved, our ticketing system will proceed to close the ticket.
          </p>
